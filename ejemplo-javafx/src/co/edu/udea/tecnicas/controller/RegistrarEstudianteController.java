package co.edu.udea.tecnicas.controller;

import co.edu.udea.tecnicas.model.Estudiante;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

public class RegistrarEstudianteController {

    @FXML
    private TextField txtIdentificacion;
    @FXML
    private TextField txtNombres;
    @FXML
    private TextField txtApellidos;
    @FXML
    private TextField txtProgramaAcademico;

    @FXML
    public void initialize(){
        txtIdentificacion.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 12) {
                return change;
            }
            return null;
        }));

    }


    public void btnGuardar_action(){
        String idIngresado = txtIdentificacion.getText();
        String nombresIngresados = txtNombres.getText();
        String apellidosIngresados = txtApellidos.getText();
        String programaAcademicoIngresado = txtProgramaAcademico.getText();
        System.out.println("Me cliquearon : "+ idIngresado+ " "+nombresIngresados);

        Estudiante estudiante = new Estudiante(idIngresado, nombresIngresados, apellidosIngresados, programaAcademicoIngresado);
        //todo ir al negocio a guardar estudiante
    }

}
