package co.edu.udea.tecnicas.model;

public class Estudiante {

    // atributos
    private String id;
    private String nombres;
    private String apellidos;
    private String programaAcademico;

    public Estudiante(String id, String nombres, String apellidos, String programaAcademico) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.programaAcademico = programaAcademico;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getProgramaAcademico() {
        return programaAcademico;
    }

    public void setProgramaAcademico(String programaAcademico) {
        this.programaAcademico = programaAcademico;
    }
}
