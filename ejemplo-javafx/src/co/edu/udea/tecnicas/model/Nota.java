package co.edu.udea.tecnicas.model;

public class Nota {

    //atributos
    private double porcentaje;
    private double valor;
    private String descripcion;

    public Nota(double porcentaje, double valor, String descripcion) {
        this.porcentaje = porcentaje;
        this.valor = valor;
        this.descripcion = descripcion;
    }

    public double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
