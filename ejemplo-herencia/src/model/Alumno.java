package model;

public class Alumno extends Persona{

    private String programaAcademico;
    private double promedioAcademico;


    public Alumno(String programaAcademico, double promedioAcademico) {
        super();
        this.programaAcademico = programaAcademico;
        this.promedioAcademico = promedioAcademico;
    }

    public String getProgramaAcademico() {
        return programaAcademico;
    }

    public void setProgramaAcademico(String programaAcademico) {
        this.programaAcademico = programaAcademico;
    }

    public double getPromedioAcademico() {
        return promedioAcademico;
    }

    public void setPromedioAcademico(double promedioAcademico) {
        this.promedioAcademico = promedioAcademico;
    }
}
