import java.time.LocalDateTime;

public class Revision {

    private LocalDateTime fecha;
    private String diagnostico;


    //asociación
    private Vehiculo vehiculo;

    public Revision(LocalDateTime fecha, String diagnostico) {
        this.fecha = fecha;
        this.diagnostico = diagnostico;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }


    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    @Override
    public String toString() {
        return "Revision{" +
                "fecha=" + fecha +
                ", diagnostico='" + diagnostico + '\'' +
                ", vehiculo=" + vehiculo +
                '}';
    }
}
