public abstract class Persona extends Humano implements Volador {
    private String id;
    private String nombres;
    private String apellidos;

    public String presentarse() {
        return String.format("Hola soy %s %s", nombres, apellidos);
    }

    public abstract String decirProfesion();

    public String volar() {
        return "vuelo como persona";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
}
