public class Gato extends Animal {

    public static int CONTADOR_GATOS = 0;

    public Gato() {
        System.out.println("Gatos creados " + ++CONTADOR_GATOS);
    }

    @Override
    public String hacerRuido() {
        return "meow!";
    }

    public String daniarMuebles() {
        return "mueble destrozado";
    }
}
