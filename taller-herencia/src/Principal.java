import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Principal {

    private static List<Animal> animales = new ArrayList();

    public static void main(String[] args) {
        String menu = "Bienvenido: \n" +
                "1. Crear Humano.\n" +
                "2. Crear Gato.\n" +
                "3. Hacer Ruido.\n" +
                "4. Trabajar.\n" +
                "5. Dañar muebles.\n" +
                "6. Terminar";

        String opcion = "";
        do {
            opcion = JOptionPane.showInputDialog(menu);

            switch (opcion) {
                case "1": //crear humano
                    Humano h = new Humano();
                    animales.add(h);
                    break;
                case "2": //crear gato
                    Gato g = new Gato();
                    animales.add(g);
                    break;
                case "3"://hacer ruido
                    // animales.forEach(Animal::hacerRuido);
                    /* for(int i=0; i<animales.size();i++){
                        System.out.println(animales.get(i).hacerRuido());
                    }*/
                    for (Animal animal : animales) {
                        System.out.println(animal.hacerRuido());
                    }
                    break;
                case "4"://trabajar
                    for (Animal animal : animales) {
                        if (animal instanceof Humano) {
                            Humano animalConvertidoHumano = (Humano) animal;
                            System.out.println(animalConvertidoHumano.trabajar());
                        }
                    }
                    break;
                case "5"://dañar muebles
                    for (Animal animal : animales) {
                        if (animal instanceof Gato) {
                            Gato animalConvertidoGato = (Gato) animal;
                            System.out.println(animalConvertidoGato.daniarMuebles());
                        }
                    }
                    break;
                case "6":
                    System.out.println("suerte");
            }

        } while (!"6".equals(opcion));
    }
}
