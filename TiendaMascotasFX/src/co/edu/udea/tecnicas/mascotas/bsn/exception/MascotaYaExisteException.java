package co.edu.udea.tecnicas.mascotas.bsn.exception;

public class MascotaYaExisteException extends Exception {

    @Override
    public String getMessage() {
        return "La mascota a almacenar ya estaba presente";
    }
}
