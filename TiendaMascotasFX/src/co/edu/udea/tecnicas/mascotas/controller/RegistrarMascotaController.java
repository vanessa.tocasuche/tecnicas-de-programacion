package co.edu.udea.tecnicas.mascotas.controller;

import co.edu.udea.tecnicas.mascotas.bsn.MascotaBsn;
import co.edu.udea.tecnicas.mascotas.bsn.exception.MascotaYaExisteException;
import co.edu.udea.tecnicas.mascotas.controller.base.BaseController;
import co.edu.udea.tecnicas.mascotas.model.Mascota;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class RegistrarMascotaController extends BaseController {

    @FXML
    private TextField txtId;
    @FXML
    private TextField txtRaza;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtTipo;

    private MascotaBsn mascotaBsn = new MascotaBsn();


    @FXML
    private void btnGuardar_action() {
        String id = txtId.getText();
        String raza = txtRaza.getText();
        String nombre = txtNombre.getText();
        String tipo = txtTipo.getText();
        boolean formularioValido = validarCampos(raza, nombre, tipo);
        if (formularioValido) {

            Mascota mascota = new Mascota(id.trim(), raza.trim(), nombre.trim(), tipo.trim());
            mascotaBsn.guardarMascota(mascota);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Registro de mascotas");
            alert.setHeaderText("Mascota creada correctamente");
            alert.setContentText(mascota.toString());
            alert.showAndWait();
            limpiarCampos();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de mascotas");
            alert.setHeaderText("Hay campos vacíos");
            alert.setContentText("Diligencie todos los campos por favor");
            alert.showAndWait();

        }
    }

    private boolean validarCampos(String... campos) {
        boolean valido = true;
        for (String campo : campos) {
            if (campo == null || "".equals(campo.trim())) {
                valido = false;
                break;
            }
        }
        return valido;
    }

    @FXML
    private void limpiarCampos() {
        txtId.setText("");
        txtNombre.setText("");
        txtRaza.setText("");
        txtTipo.setText("");
    }

    @Override
    public void procesarMensaje(Object mensaje) {
        if (mensaje != null) {
            Mascota mascota = (Mascota) mensaje;
            txtId.setText(mascota.getId());
            txtNombre.setText(mascota.getNombre());
            txtTipo.setText(mascota.getTipo());
            txtRaza.setText(mascota.getRaza());
        }
    }
}
