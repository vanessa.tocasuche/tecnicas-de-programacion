package co.edu.udea.tecnicas.mascotas.dao.exception;

public class LlaveDuplicadaException extends Exception {

    @Override
    public String getMessage() {
        return "La llave ya existía";
    }
}
