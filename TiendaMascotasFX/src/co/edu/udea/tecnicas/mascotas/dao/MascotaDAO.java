package co.edu.udea.tecnicas.mascotas.dao;

import co.edu.udea.tecnicas.mascotas.dao.exception.LlaveDuplicadaException;
import co.edu.udea.tecnicas.mascotas.model.Mascota;

import java.util.List;

public interface MascotaDAO {
    void guardarMascota(Mascota mascota);
    List<Mascota> listarMascotas();

}
