package co.edu.udea.tecnicas.mascotas.dao.impl;

import co.edu.udea.tecnicas.mascotas.dao.MascotaDAO;
import co.edu.udea.tecnicas.mascotas.model.Mascota;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MascotaDAOList implements MascotaDAO {

    private static List<Mascota> mascotas = new ArrayList();

    @Override
    public void guardarMascota(Mascota mascota) {
        //todo validar reglas de bd (integridad de la información)
        Optional<Mascota> respuesta = buscarMascotaPorId(mascota.getId());
        if (respuesta.isPresent()) {
            mascotas.remove(respuesta.get());
        }
        mascotas.add(mascota);
    }

    @Override
    public List<Mascota> listarMascotas() {
        return mascotas;
    }

    private Optional<Mascota> buscarMascotaPorId(String id) {
        Optional<Mascota> mascotaOptional = Optional.empty();
        for (Mascota mascota : mascotas) {
            if (id.equals(mascota.getId())) {
                mascotaOptional = Optional.of(mascota);
                break;
            }
        }
        return mascotaOptional;
    }
}
