import model.Curso;
import model.Estudiante;
import model.Nota;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Principal {

    private static List<Curso> cursos = new ArrayList();
    private static List<Estudiante> estudiantes = new ArrayList();

    public static void main(String[] args) {
        String menu = "Bienvenido\n" +
                "1. Registrar Cursos\n" +
                "2. Registrar Estudiantes\n" +
                "3. Registrar Nota\n" +
                "4. Ver promedio de notas (Estudiante)"+
                "5. Ver promedio de notas (Curso)"+
                "4. Salir";

        String opcion = "";

        do {
            opcion = JOptionPane.showInputDialog(menu);

            switch (opcion) {
                case "1":
                    registrarCurso();
                    break;
                case "2":
                    registrarEstudiante();
                    break;
                case "3":
                    registrarNota();
                    break;
                case "4":
                    obtenerPromedioNotasEstudiante();
                    break;
                case "5":
                    obtenerPromedioNotasCurso();
                    break;
                case "6":
                    JOptionPane.showMessageDialog(null, "suerte");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opción incorrecta ¬¬'");
            }

        } while (!"6".equals(opcion));
    }

    private static void obtenerPromedioNotasCurso() {
        //se consulta al curso por código
        int codigoAConsultar = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el código del curso:"));
        Optional<Curso> cursoOptional = Optional.empty();
        for (Curso curso : cursos) {
            if (curso.getCodigo() == codigoAConsultar) {
                cursoOptional = Optional.of(curso);
                break;
            }
        }
        if (!cursoOptional.isPresent()) {
            JOptionPane.showMessageDialog(null, "No existe el curso con código: " + codigoAConsultar);
            return;
        }

        Curso curso = cursoOptional.get();
        double promedio = curso.obtenerPromedio();
        JOptionPane.showMessageDialog(null, "El promedio del curso es de: " + promedio);

    }

    private static void obtenerPromedioNotasEstudiante() {
        //se consulta al estudiante por id
        String idAConsultar = JOptionPane.showInputDialog("Ingrese el id del estudiante:");
        Optional<Estudiante> estudianteOptional = Optional.empty();
        for (Estudiante estudiante : estudiantes) {
            if (estudiante.getId().equals(idAConsultar)) {
                estudianteOptional = Optional.of(estudiante);
                break;
            }
        }
        if (!estudianteOptional.isPresent()) {
            JOptionPane.showMessageDialog(null, "No existe el estudiante con id: " + idAConsultar);
            return;
        }

        Estudiante estudiante = estudianteOptional.get();
        double promedio = estudiante.obtenerPromedio();
        JOptionPane.showMessageDialog(null, "El promedio del estudiante es de: " + promedio);

    }

    private static void registrarCurso() {
        int codigo = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el código:"));
        String nombre = JOptionPane.showInputDialog("Ingrese el nombre:");
        int numeroCreditos = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número de créditos:"));

        Curso curso = Curso.builder()
                .withCodigo(codigo)
                .withNombre(nombre)
                .withNumeroCreditos(numeroCreditos)
                .build();
        cursos.add(curso);
        JOptionPane.showMessageDialog(null, "El curso fue registrado correctamente");
    }

    private static void registrarEstudiante() {
        String id = JOptionPane.showInputDialog("Ingrese el id:");
        String nombres = JOptionPane.showInputDialog("Ingrese los nombres:");
        char genero = JOptionPane.showInputDialog("Ingrese el género:").charAt(0);
        int programaAcademico = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el programa:"));

        Estudiante estudiante = Estudiante.builder()
                .withId(id)
                .withNombres(nombres)
                .withGenero(genero)
                .withProgramaAcademico(programaAcademico)
                .build();
        estudiantes.add(estudiante);
        JOptionPane.showMessageDialog(null, "El estudiante fue registrado correctamente");
    }

    private static void registrarNota() {
        //se consulta al estudiante por id
        String idAConsultar = JOptionPane.showInputDialog("Ingrese el id del estudiante:");
        Optional<Estudiante> estudianteOptional = Optional.empty();
        for (Estudiante estudiante : estudiantes) {
            if (estudiante.getId().equals(idAConsultar)) {
                estudianteOptional = Optional.of(estudiante);
                break;
            }
        }
        if (!estudianteOptional.isPresent()) {
            JOptionPane.showMessageDialog(null, "No existe el estudiante con id: " + idAConsultar);
            return;
        }

        //se consulta al curso por código
        int codigoAConsultar = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el código del curso:"));
        Optional<Curso> cursoOptional = Optional.empty();
        for (Curso curso : cursos) {
            if (curso.getCodigo() == codigoAConsultar) {
                cursoOptional = Optional.of(curso);
                break;
            }
        }
        if (!cursoOptional.isPresent()) {
            JOptionPane.showMessageDialog(null, "No existe el curso con código: " + codigoAConsultar);
            return;
        }

        double valor = Double.parseDouble(JOptionPane.showInputDialog("Ingrese la nota obtenida: "));

        //se extraen los objetos almacenados en el Optional
        Estudiante estudiante = estudianteOptional.get();
        Curso curso = cursoOptional.get();

        Nota nota = new Nota(valor, estudiante, curso);

        //se agrega la nota al estudiante y curso correspondiente
        estudiante.getNotas().add(nota);
        curso.getNotas().add(nota);

        JOptionPane.showMessageDialog(null, "La nota fue registrada correctamente");

    }
}
