package model;

import model.builder.EstudianteBuilder;

import java.util.ArrayList;
import java.util.List;

public class Estudiante {

    //atributos
    private String id;
    private String nombres;
    private char genero;
    private int programaAcademico;

    //asociaciones
    private List<Nota> notas = new ArrayList();

    public Estudiante(){

    }

    public Estudiante(String id, String nombres, char genero, int programaAcademico) {
        this.id = id;
        this.nombres = nombres;
        this.genero = genero;
        this.programaAcademico = programaAcademico;
    }

    public double obtenerPromedio() {
        double suma = 0;
        for (Nota nota : notas) {
            suma += nota.getValor();
        }
        return suma / notas.size();
    }

    public static EstudianteBuilder builder(){
        return new EstudianteBuilder();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public int getProgramaAcademico() {
        return programaAcademico;
    }

    public void setProgramaAcademico(int programaAcademico) {
        this.programaAcademico = programaAcademico;
    }

    public List<Nota> getNotas() {
        return notas;
    }

    public void setNotas(List<Nota> notas) {
        this.notas = notas;
    }
}
