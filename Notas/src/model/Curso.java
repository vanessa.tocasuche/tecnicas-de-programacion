package model;

import model.builder.CursoBuilder;

import java.util.ArrayList;
import java.util.List;

public class Curso {

    //atributos
    private int codigo;
    private String nombre;
    private int numeroCreditos;

    //asociaciones
    private List<Nota> notas=new ArrayList();

    public Curso() {
    }

    public Curso(int codigo, String nombre, int numeroCreditos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.numeroCreditos = numeroCreditos;
    }

    public static CursoBuilder builder() {
        return new CursoBuilder();
    }

    public double obtenerPromedio() {
        double suma = 0;
        for (Nota nota : notas) {
            suma += nota.getValor();
        }
        return suma / notas.size();
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumeroCreditos() {
        return numeroCreditos;
    }

    public void setNumeroCreditos(int numeroCreditos) {
        this.numeroCreditos = numeroCreditos;
    }

    public List<Nota> getNotas() {
        return notas;
    }

    public void setNotas(List<Nota> notas) {
        this.notas = notas;
    }
}
