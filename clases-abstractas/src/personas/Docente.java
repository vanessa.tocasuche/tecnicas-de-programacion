package personas;

public class Docente extends Empleado {

    private String areaProfundizacion;

    public Docente(String identificacion, String nombres, String apellidos, int rangoSalarial, String areaProfundizacion) {
        super(identificacion, nombres, apellidos, rangoSalarial);
        this.areaProfundizacion = areaProfundizacion;
    }

    @Override
    public String decirProfesion() {
        return "Soy docente";
    }
}
