import personas.Estudiante;
import personas.Medico;
import personas.Persona;

public class Principal {

    public static void main(String[] args) {
        //Persona p = new Persona("123", "aaaa", "bbb");
        //System.out.println(p.presentarse());

        Persona medico = new Medico("123", "aaa", "bbb", 3, "Pediatría");
        Persona estudiante = new Estudiante("456", "bbb", "ccc", 3.8, "Ingeniería de Sistemas");

        System.out.println(medico.decirProfesion());
        System.out.println(estudiante.decirProfesion());


    }
}
