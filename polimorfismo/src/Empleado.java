public class Empleado {

    // atributo de clase (variable global)
    private static double PORCENTAJE_DESCUENTO = 0.1;

    // atributos propios a cada empleado
    private String nombre;
    private double  valorHora;

    public Empleado(String nombre, double valorHora) {
        this.nombre = nombre;
        this.valorHora = valorHora;
    }

   public String calcularSalario(int cantidadHoras){
        double salarioBruto = cantidadHoras*valorHora; //valor bruto
        double salarioConDescuento = salarioBruto*(1-PORCENTAJE_DESCUENTO);

        String mensaje = nombre + " GANA: $"+salarioConDescuento;
        return mensaje;
   }
}
