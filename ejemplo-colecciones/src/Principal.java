import java.util.*;
import java.util.stream.Collectors;

public class Principal {

    public static void main(String[] args) {
        mostrar("oee", "aaa", "eeee");

        Collection<Integer> coleccion = new LinkedList();
        coleccion.add(2);
        coleccion.add(4);
        coleccion.add(2);
        coleccion.add(6);
        coleccion.add(2);
        coleccion.add(4);
        coleccion.add(2);
        coleccion.add(6);
        coleccion.add(2);
        coleccion.add(4);
        coleccion.add(2);
        coleccion.add(6);
        System.out.println(coleccion.size());

        List<Integer> lista = (List) coleccion;
        System.out.println(lista.size());

        for (int i = 0; i < coleccion.size(); i++) {
            Integer valor = lista.get(i);
            if (valor > 2) {
                System.out.println(valor);
            }
        }

        for (Integer valor : lista) {
            if (valor > 2) {
                System.out.println(valor);
            }
        }

        List<Integer> respuesta = coleccion
                .stream()
                .filter(numero -> numero >= 4)
                .collect(Collectors.toList());

        System.out.println(respuesta);


        Map<String, Integer> mapa = new TreeMap();
        mapa.put("palabra1", 3);
        mapa.put("palabra2", 4);
        mapa.put("palabra1", 1);

        Integer valor = mapa.get("palabra1");
        System.out.println(valor);


    }


    public static void mostrar(String... mensajes) {
        for (int i = 0; i < mensajes.length; i++) {
            System.out.println(mensajes[i]);
        }
    }
}
